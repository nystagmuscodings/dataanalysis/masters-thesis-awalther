# Filename: ExpUtils.py
#
# This file contains methods, that may be reused in any kind of experiment.
#
################################################################################
import pylink
import os
from psychopy import core, event, monitors, visual
from EyeLinkCoreGraphicsPsychoPy import EyeLinkCoreGraphicsPsychoPy
################################################################################


##
# This method creates a window instance with the given width and height.
#
# param width - The width the window instance shall have.
# param height - The height the window instance shall have.
#
# return - A window instance with the given width and height.
##
def createWindow(width, height):
    # We need to set monitor parameters to use the different PsychoPy screen "units".
    mon = monitors.Monitor('myMonitor', width=53.0, distance=70.0)
    mon.setSizePix((width, height))

    # open a window; set winType='pyglet' to prevent text display issues in PsychoPy2
    return visual.Window((width, height), fullscr=True, monitor=mon, winType='pyglet', units='pix', color = (1,1,1))


##
# This method performs the initial setup of the eyetracker.
# The setup includes establishing a connection to '100.1.1.1' and sending initial
# metadata to the EDF file with the given dataFileName.
#
# param dataFileName - The name the generated EDF file shall have.
# param screenWidth - The width of the used screen. Will be sent to the EDF file.
# param screenHeight - The height of the used screen. Will be sent to the EDF file.
#
# return - The created eyetracker instance.
##
def setupTracker(dataFileName, screenWidth, screenHeight):
    # Open a connection to the tracker. Use None for dummy mode or '100.1.1.1' for recordings.
    tracker = pylink.EyeLink('100.1.1.1')
    tracker.openDataFile(dataFileName)
    # Add personalized data file header (preamble text).
    tracker.sendCommand("add_file_preamble_text 'Nystagmus Experiment'")
    # Put the tracker in idle mode before we change its parameters.
    tracker.setOfflineMode()
    pylink.pumpDelay(100)

    # IMPORTANT: Send screen resolution to the tracker.
    tracker.sendCommand("screen_pixel_coords = 0 0 %d %d" % (screenWidth-1, screenHeight-1))

    # Save screen resolution in EDF data, so Data Viewer can correctly load experimental graphics.
    tracker.sendMessage("DISPLAY_COORDS = 0 0 %d %d" % (screenWidth-1, screenHeight-1))

    # Sampling rate, 250, 500, 1000, or 2000.
    tracker.sendCommand("sample_rate 1000")

    # Detect eye events based on "GAZE" (or "HREF") data.
    tracker.sendCommand("recording_parse_type = GAZE")

    # Saccade detection thresholds: 0-> standard/coginitve, 1-> sensitive/psychophysiological.
    tracker.sendCommand("select_parser_configuration 0") 

    # Choose a calibration type, H3, HV3, HV5, HV9, HV13 (HV = horiztonal/vertical).
    tracker.sendCommand("calibration_type = HV5") 

    # Tracker hardware, 1-EyeLink I, 2-EyeLink II, 3-Newer models (1000/1000Plus/Portable DUO).
    hardware_ver = tracker.getTrackerVersion()

    # Get tracking software version.
    software_ver = 0
    if hardware_ver == 3:
        tvstr = tracker.getTrackerVersionString()
        vindex = tvstr.find("EYELINK CL")
        software_ver = float(tvstr.split()[-1])

    # Define the to be saved sample and event data.
    tracker.sendCommand("file_event_filter = LEFT,RIGHT,FIXATION,SACCADE,BLINK,MESSAGE,BUTTON,INPUT")
    if software_ver >= 4:
        tracker.sendCommand("file_sample_data  = LEFT,RIGHT,GAZE,GAZERES,PUPIL,HREF,AREA,STATUS,HTARGET,INPUT")
    else:
        tracker.sendCommand("file_sample_data  = LEFT,RIGHT,GAZE,GAZERES,PUPIL,HREF,AREA,STATUS,INPUT")

    # Define the sample and event data that will be available over the link.
    tracker.sendCommand("link_event_filter = LEFT,RIGHT,FIXATION,FIXUPDATE,SACCADE,BLINK,BUTTON,INPUT")
    if software_ver >= 4:
        tracker.sendCommand("link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,PUPIL,HREF,AREA,STATUS,HTARGET,INPUT")
    else:
        tracker.sendCommand("link_sample_data  = LEFT,RIGHT,GAZE,GAZERES,PUPIL,HREF,AREA,STATUS,INPUT")

    return tracker


##
# This method performs the initial calibration of the eyetracker.
#
# param tracker - The to be calibrated eyetracker instance.
# param window - The window in which the calibration targets shall be displayed.
##
def setupCalibration(tracker, window):
    # Set up a custom graphics envrionment (EyeLinkCoreGraphicsPsychopy) for calibration.
    gEnv = EyeLinkCoreGraphicsPsychoPy(tracker, window)
    # Define stimulus shape.
    gEnv.calTarget = 'circle'
    pylink.openGraphicsEx(gEnv)


##
# This method performs the actual calibration of the eyetracker.
#
# param window - The window in which the instructions shall be displayed.
# param tracker - The to be calibrated eyetracker.
##
def doCalibration(window, tracker):
    # Instruct the experimenter to calibrate the eye tracker.
    showText(window, 'Drücke 2x ENTER um den Eyetracker zu kalibrieren.')

    # Set up the camera and calibrate the tracker.
    tracker.doTrackerSetup()


##
# This method is used to display the given text in the given window. The text
# will be printed in black, with a font size of 50px and will span across the whole screen.
# The displayed screen can be exited with any key press.
#
# param window - The window in which the given text shall be displayed.
# param displayedText - The to be displayed text.
##
def showText(window, displayedText):
    instrText = visual.TextStim(window, displayedText, color='black', height = 50, wrapWidth = 1400)
    instrText.draw()
    window.flip()
    event.waitKeys()


##
# This method handles everthing that has to be done before the eye recording starts.
# This includes sending a message with the given trialID to the EDF file,
# performing a drift check and starting the actual recording.
#
# param tracker - The used eyetracker instance.
# param trialIndex - The number of the current trial. Will be sent to EDF file.
# param screenCenter - The center coordinates of the screen. Used to display stimulus in drift check.
##
def preRecording(window, tracker, trialIndex, targetPos):
     # Send the standard "TRIALID" message to mark the start of a trial.
    tracker.sendMessage('TRIALID %d' % trialIndex)
    stimulus = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    stimulus.pos = targetPos
    stimulus.draw()
    window.flip()

    event.waitKeys()

    # Put the tracker in idle mode before we start recording.
    tracker.setOfflineMode()
    pylink.pumpDelay(100)

    # Arguments: sample_to_file, events_to_file, sample_over_link, event_over_link (1-yes, 0-no)
    err = tracker.startRecording(1, 1, 1, 1)
    pylink.pumpDelay(100)  # wait for 100 ms to cache some samples


##
# This method handles everthing that has to be done after the actual recording.
# This includes clearing the screen, stopping the recording and sending the
# trial results to the EDF file.
#
# param window - The to be cleared window.
# param tracker - The used eyetracker instance.
##
def postRecording(window, tracker):
    # Clear the screen.
    window.color=[1, 1, 1]
    window.flip()
    tracker.sendMessage('blank_screen')

    # Stop recording.
    tracker.stopRecording()
    # Clear the host display, this command is needed if you have backdrop image on the Host.
    tracker.sendCommand('clear_screen 0')

    # Send over the standard 'TRIAL_RESULT' message to mark the end of trial.
    tracker.sendMessage('TRIAL_RESULT 0')


##
# This method handles everything that needs to be done after the experiment is done.
# This includes the closure of the EDF file and the shutdown of the tracker.
#
# param tracker - The used eyetracker instance.
# param window - Used to display any messages.
# param dataFileName - The name of the EDF file.
##
def tearDown(tracker, window, dataFileName):
    showText(window, 'Das Experiment wurde beendet.')
    # Close the EDF data file and put the tracker in idle mode.
    tracker.setOfflineMode()
    pylink.pumpDelay(100)
    tracker.closeDataFile()

    # Download EDF file to Display PC and put it in local folder ('edfData').
    # Tell the user about the process.
    showText(window, 'EDF Daten werden vom Eyelink auf den Host-PC übertragen...')
    pylink.pumpDelay(500)

    # Make sure the 'edfData' folder is there. Create one if not.
    dataFolder = os.getcwd() + '/edfData/'
    if not os.path.exists(dataFolder): 
        os.makedirs(dataFolder)
    tracker.receiveDataFile(dataFileName, 'edfData' + os.sep + dataFileName)

    # Close the connection to the tracker.
    tracker.close()
    
    core.quit()