# -*- coding: UTF-8 -*-
##############################################################################
# CustomClass Rules                                                          #
# =================                                                          #
#                                                                            #
#1. All custom classes must inherit sreb.EBObject and the constructor        #
#   must call sreb.EBObject's constructor. If a class starts with _ then it  #
#   is considered internal and will not be treated as a custom class.        #
#                                                                            #
#2. The custom class will only use the default constructor.                  #  
#   ie. a constructor with no parameters.                                    #
#                                                                            #
#3. To add a property provide getter and have an attribute that does not     #
#   starts with an underscore(_) or an upper case letter, and have the       #
#   attribute of a know type. Known types are int,float,str,EBPoint,EBColor, #
#   tuple, and list.                                                         #
#   If an attribute's default value is of type tuple and only has two        #
#   items, the property will be treated as an EBPoint. Similarly, if an      #
#   attribute's default value is a tuple of 3 items the property will be     #
#   treated as an EBColor.  The input type of the setter and the output type #
#   of the getter is expected to be the same as the type of the attribute.   #
#                                                                            #
#4. If only getter is provided, the property will be a readonly property.    # 
#                                                                            #
#6. The custom class may be instanciated during development to obtain        # 
#   class info. Avoid such things as display mode change, remote connections # 
#   in the constructor.                                                      #
#                                                                            # 
#7. Any method in the custom class can be called using the Execute action    #
#   By default, the return type of the method is string unless a doc string  #
#   with the following constraint is available                               #
#	a. The doc string starts with "RETURN:" (case matters)               #
#       b. Following the text "RETURN:" provide a default value of the type  #
#          or the __repr__ value of the class. eg. str for string            #
#8. If a property's setter metthod has default values for it's parameters,   #
#    the property will not accept references or equation.                    #
##############################################################################


import sreb



#
# This class contains all methods used to implement the extended gaze contigency of a displayed image.
#
class ContMethods(sreb.EBObject):

	#
	# Standard init function
	#
	def __init__(self):
		sreb.EBObject.__init__(self)
		
		# The gaze position of the end of a fast eye movement.
		self.cachedGaze = (512, 384)

		
	#
	# This method computes the position of the displayed image, according to the given gaze samples and
	# the cached position of the last fast eye movements end position.
	#
	def computeImgLocation(self, eyeX, eyeY):
		"""RETURN:(512, 384)"""

		return (eyeX - self.cachedGaze[0] + 512 , eyeY - self.cachedGaze[1] + 384)
	

	#
	# Getter and setter for the cached gaze position of the end of fast eye movement.
	#
	def setCachedGaze(self, value):
		self.cachedGaze = value

	def getCachedGaze(self):
		return self.cachedGaze
