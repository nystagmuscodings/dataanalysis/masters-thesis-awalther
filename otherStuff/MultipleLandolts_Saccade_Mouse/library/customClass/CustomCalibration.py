# -*- coding: UTF-8 -*-
##############################################################################
# CustomClass Rules                                                          #
# =================                                                          #
#                                                                            #
#1. All custom classes must inherit sreb.EBObject and the constructor        #
#   must call sreb.EBObject's constructor. If a class starts with _ then it  #
#   is considered internal and will not be treated as a custom class.        #
#                                                                            #
#2. The custom class will only use the default constructor.                  #  
#   ie. a constructor with no parameters.                                    #
#                                                                            #
#3. To add a property provide getter and have an attribute that does not     #
#   starts with an underscore(_) or an upper case letter, and have the       #
#   attribute of a know type. Known types are int,float,str,EBPoint,EBColor, #
#   tuple, and list.                                                         #
#   If an attribute's default value is of type tuple and only has two        #
#   items, the property will be treated as an EBPoint. Similarly, if an      #
#   attribute's default value is a tuple of 3 items the property will be     #
#   treated as an EBColor.  The input type of the setter and the output type #
#   of the getter is expected to be the same as the type of the attribute.   #
#                                                                            #
#4. If only getter is provided, the property will be a readonly property.    # 
#                                                                            #
#6. The custom class may be instanciated during development to obtain        # 
#   class info. Avoid such things as display mode change, remote connections # 
#   in the constructor.                                                      #
#                                                                            # 
#7. Any method in the custom class can be called using the Execute action    #
#   By default, the return type of the method is string unless a doc string  #
#   with the following constraint is available                               #
#	a. The doc string starts with "RETURN:" (case matters)               #
#       b. Following the text "RETURN:" provide a default value of the type  #
#          or the __repr__ value of the class. eg. str for string            #
#8. If a property's setter metthod has default values for it's parameters,   #
#    the property will not accept references or equation.                    #
##############################################################################


import sreb



class CustomClassTemplate(sreb.EBObject):
	def __init__(self):
		sreb.EBObject.__init__(self)

	#
	#Callable method using Execute action. 
	#Note the default arguments and the doc string to let eb know what is the expected return type.
	#def computePointOfReference(self):
		#"""RETURN:[1.11,1.11]"""
		#eng = matlab.engine.start_matlab()
		#return eng.mjd_launch('analyse','nystagmus_findpointofregard','inputFile','C:\Users\ingpsy\Downloads\mjd_nystools_1.0\example_data.csv','startTime_secs',0,'endTime_secs',Inf,'eyeToAnalyse','Right','SaccadeDefinition__rejectSaccadesBelowAmplitude',0.5000,'BlinkDefinition__removeBlinks',1,'BlinkDefinition__considerLargeMovementsBlinks',1,'BlinkDefinition__timeRemovedEitherSide_ms',75);
	
	
