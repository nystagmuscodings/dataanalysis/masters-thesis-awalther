# Filename: NysExp.py
#
# This file contains the main logic for the nystagmus experiment.
#
################################################################################
from psychopy import event, visual, core
from ExpUtils import *
from NysExpHelpFunctions import *
from CustomCalibration import *
################################################################################


##########     SETUP     ##########

# Create the name of the EDF file and check what type of experiment shall be run.
dataFileName, useMouse, expType, useAlgorithm = getExpInfo()
# Define the dimensions of the to be used screen.
screenWidth, screenHeight = (1920, 1080)
screenCenter = (int(screenWidth/2), int(screenHeight/2))
# Create a new window instance. window.getActualFrameRate() returned ca. 120Hz.
window = createWindow(screenWidth, screenHeight)
# Setup the eye tracker or mouse.
if useMouse:
    mouse = event.Mouse(True, None, window)
else:
    tracker = setupTracker(dataFileName, screenWidth, screenHeight)
    # Setup the eye tracker calibration.
    setupCalibration(tracker, window)
randomTrialDataEyetest, practiseTrialData, randomTrialDataImg = loadTrialData()


##########     CALIBRATION     ##########

if not useMouse and useAlgorithm:
    doCalibration(window, tracker)
    #if doCustomCal(window, tracker):
    #    tearDown(tracker, window, dataFileName)


##########     PARAMETER EXTRACTION     ##########

if not useMouse and useAlgorithm:
    if extractParameters(window, tracker):
        tearDown(tracker, window, dataFileName)

##########     INSTRUCTIONS     ##########

displayInstructions(window)


##########     RUN TRIALS     ##########

# Run practise trials, until at least two answers were correct.
if useMouse:
    if runPractiseTrial(practiseTrialData, window, mouse, screenCenter, useMouse):
        showText(window, 'Das Experiment wurde gestoppt.')
        core.quit()
else:
    if runPractiseTrial(practiseTrialData, window, tracker, screenCenter, useMouse):
        tearDown(tracker, window, dataFileName)


# Start the real experiment and display instructions again.
showText(window, 'Es beginnt nun das richtige Experiment.')
displayInstructions(window)
if expType == 'Eyetest':
    if useMouse:
        doEyetestTrials(window, mouse, screenCenter, useMouse, randomTrialDataEyetest)
    else:
        doEyetestTrials(window, tracker, screenCenter, useMouse, randomTrialDataEyetest)
elif expType == 'Image':
    if useMouse:
        doImgTrials(window, mouse, screenCenter, useMouse, randomTrialDataImg)
    else:
        doImgTrials(window, tracker, screenCenter, useMouse, randomTrialDataImg)


##########     TEAR DOWN     ##########

if not useMouse:
    tearDown(tracker, window, dataFileName)
else:
    showText(window, 'Das Experiment ist beendet.')
    core.quit()