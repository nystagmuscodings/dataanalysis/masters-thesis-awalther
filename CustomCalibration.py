# Filename: CustomCalibration.py
#
# This file contains methods for a custom eyetracker calibration.
#
################################################################################
from psychopy import visual, core
from psychopy.hardware import keyboard
import collections
from ExpUtils import preRecording, showText
import NysExpHelpFunctions as nysHelp
from statistics import median
import pylink
from math import hypot
################################################################################


def doCustomCal(window, tracker):
    showText(window, 'Im Folgenden werden einige Parameter Ihres Nystagmus bestimmt.\n' +
                    'Dazu werden Ihnen nacheinander rote Punkte präsentiert.\n' +
                    'Bitte versuchen Sie diese so gut wie möglich zu fixieren.')
    keyBoard = keyboard.Keyboard()

    # Prepare stimuli.
    absStimPosX = int(window.monitor.getSizePix()[0] / 4)
    absStimPosY = int(window.monitor.getSizePix()[1] / 4)
    # The positions the stimuli will be placed at: Center, Top, Right, Bottom, Left.
    stimPositions = [(0,0), (0, absStimPosY), (absStimPosX, 0), (0, -absStimPosY), (-absStimPosX, 0)]
    stimulus = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    stimulus.pos = stimPositions[0]
    # This counter counts the number of stimuli that were already presented.
    stimCount = 0

    # Setup stimulus for simulation.
    #simStim = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim.pos = stimPositions[0]
    #simPendular = False
    #moveRight = True
    # Buffer for saccade amplitudes during fixations.
    fixationAmpBuffer = collections.deque()
    # Buffer for saccade amplitudes when moveing fixation between stimuli.
    gazeAmpBuffer = collections.deque()
    # Buffer for vertical eye movements during fixation.
    verticalMoveBuffer = collections.deque()
    # Buffer for nystagmic saccade direction.
    directionBuffer = collections.deque()
    # Flag that indicates if a fixation is assumed.
    fixation = True
    # This flag indicates if the stimulus has been moved already since the timer elapsed.
    movedStimulus = False

    # Buffers for the data that will be sent to the mjd calibration.
    eyeDataBuffer = collections.deque()
    pupilSizeBuffer = collections.deque()


    ##########     RECORDING - START     ##########

    preRecording(window, tracker, -1, (0, 0))

    stimulus.draw()
    #simStim.draw()
    window.flip()
    timer = core.CountdownTimer(10)
    smpTimeStamp = -32768

    while True:
        keys = keyBoard.getKeys(keyList = ['escape'], waitRelease=False)
        # Break out, if ESC was pressed.
        if 'escape' in keys:
            # Send over a message to log the key press.
            tracker.sendMessage('Pressed ESC. Exiting.')
            return True

        # Get the newest samle from the eyetracker and store it in the buffers.
        data = tracker.getNewestSample()
        if data is not None:
            if data.getTime() != smpTimeStamp:
                smpTimeStamp = data.getTime()
                eyeDataBuffer.append(data.getRightEye().getGaze())
                pupilSizeBuffer.append(data.getRightEye().getPupilSize())

        ev = tracker.getNextData()
        # Iterate over the eyetracker messages and check, if an end of a saccade has been detected.
        while ev != 0:
            # If the end of a saccade has been detected, append its amplitude to one of the buffers.
            if ev == pylink.ENDSACC:
                data = tracker.getFloatData()
                # Only use data of right eye.
                if data.getEye() == 1:
                    # Compute the amplitude of the saccade.
                    sacAmplitude = data.getAmplitude()
                    amp = hypot(sacAmplitude[0], sacAmplitude[1])
                    # If the to be fixated stimulus is presented, append the amplitude to the corresponding buffer.
                    if fixation:
                        if amp > 1:
                            fixationAmpBuffer.append(amp)
                            verticalMoveBuffer.append(abs(data.getStartGaze()[1] - data.getEndGaze()[1]))
                            directionBuffer.append(abs(data.getAngle()))
                    # If the stimulus positions is about to change, check if the detected saccades amplitude is much higher than the median amplitude during fixation.
                    # If so, the amplitude propably belongs to the change of stimulus fixation.
                    else:
                        gazeAmpBuffer.append(amp)
                    break
            ev = tracker.getNextData()

        timerTime = timer.getTime()
        if timerTime < 0.5 and timerTime > 0:
            fixation = False
        if timerTime < 0.01 and not movedStimulus:
            f = open('stim' + str(stimCount) + '.csv', "w")
            f.write('OMS Eye Trace,Tracker = eyelink,Sampling rate = 1000Hz,Screen resolution = (1920 1080)px,Units = degrees,,,\n')
            f.write('timestamp,re_x,re_y,re_pupil,le_x,le_y,le_pupil,message\n')
            for idx, eyeData in enumerate(eyeDataBuffer):
                f.write(str(idx) + ',' + str("%.1f" % eyeData[0]) + ',' + str("%.1f" % eyeData[1]) + ',' + str("%.0f" % pupilSizeBuffer[idx]) + ',,,,\n')
            stimCount += 1
            if stimCount > 4:
                break
            stimulus.pos = stimPositions[stimCount]
            #simStim.pos = stimPositions[stimCount]
            movedStimulus = True
        if timerTime < -0.5:
            fixation = True
            timer.reset()
            movedStimulus = False

        #if simPendular:
        #    if moveRight:
        #        simStim.pos += (1, 0)
        #        if simStim.pos[0] > stimPositions[stimCount][0] + 50:
        #            moveRight = False
        #    else:
        #        simStim.pos -= (1, 0)
        #        if simStim.pos[0] < stimPositions[stimCount][0] - 50:
        #            moveRight = True
        #else:
        #    simStim.pos += (0.5, 0)
        #    if simStim.pos[0] > stimPositions[stimCount][0] + 50:
        #        simStim.pos = stimPositions[stimCount]

        stimulus.draw()
        #simStim.draw()
        window.flip()


    ##########     RECORDING - END     ##########

    tracker.stopRecording() 

    # Extract the amplitude during fixation.
    # Check if any saccades occurred during fixation. If not, the subject has most probably pendular nystagmus.
    if fixationAmpBuffer:
        nysHelp.fixationAmplitude = median(fixationAmpBuffer)

        # Extract the frequency.
        freqHelpBuffer = collections.deque()
        for amp in fixationAmpBuffer:
            if amp > nysHelp.fixationAmplitude * 0.5 and  amp < nysHelp.fixationAmplitude * 2:
                freqHelpBuffer.append(amp)
        nysHelp.freq = len(freqHelpBuffer) / 15

        # Extract the direction.
        leftCount = 0
        rightCount = 0
        for angle in directionBuffer:
            if 135 < angle and angle < 180:
                leftCount += 1
            elif 0 < angle and angle < 45:
                rightCount += 1
        if rightCount < leftCount:
            nysHelp.direction = 'left'
            nysHelp.upperAngleBound = 180
            nysHelp.lowerAngleBound = 135
        else:
            nysHelp.direction = 'right'
            nysHelp.upperAngleBound = 45
            nysHelp.lowerAngleBound = 0

    # Check if pendular or jerk. Assume that a subject with pendular nystagmus will not
    # voluntary create saccades more than once in two seconds. One stimulus presentation lasts 2.5 seconds.
    # The complete extraction phase lasts 2.5 * 6 seconds, which leads to 2.5 * 6 / 2 acceptable voluntary
    # saccades during the extraction phase. Otherwise it is considered to be jerk nystagmus.
    nysHelp.pendularNys = not fixationAmpBuffer or len(fixationAmpBuffer) < 8

    # Extract the 6 highest values of the gaze amplitudes.
    if gazeAmpBuffer:
        gazeAmpHelpBuffer = collections.deque()
        while len(gazeAmpHelpBuffer) < 6:
            maxVal = max(gazeAmpBuffer)
            gazeAmpHelpBuffer.append(maxVal)
            gazeAmpBuffer.remove(maxVal)
        nysHelp.gazeAmplitude = median(gazeAmpHelpBuffer)

    # Extract the vertical movement during fixation.
    if verticalMoveBuffer:
        nysHelp.vertMovementPx = median(verticalMoveBuffer)

    # Save data to file.
    f = open("calData.csv", "a")
    f.write('Fixation Amplitudes: \n')
    for amp in fixationAmpBuffer:
        f.write(str(amp) + '\n')
    f.write('######################################################\n')
    f.write('Gaze Amplitudes: \n')
    for amp in gazeAmpBuffer:
        f.write(str(amp) + '\n')
    f.write('######################################################\n')
    f.write('Vertical Movements: \n')
    for mov in verticalMoveBuffer:
        f.write(str(mov) + '\n')
    f.write('######################################################\n')
    f.write('Angles: \n')
    for ang in directionBuffer:
        f.write(str(ang) + '\n')
    f.write('######################################################\n')
    f.write('Jerk or Pendular: ' + ('pendular' if nysHelp.pendularNys else 'jerk') + '\n' +
            'Fixation amplitude[°]: ' + str(nysHelp.fixationAmplitude) + '\n' +
            'Gaze amplitude[°]: ' + str(nysHelp.gazeAmplitude) + '\n' +
            'Vertical Movement[px]: ' + str(nysHelp.vertMovementPx) + '\n' +
            'Frequency[Hz]: ' + str(nysHelp.freq) + '\n' +
            'Direction: ' + nysHelp.direction)
    f.close()
    # Show results on screen.
    if nysHelp.pendularNys:
        showText(window, 'Pendular Nystagmus\n' +
                        'Gaze amplitude[°]: ' + str(nysHelp.gazeAmplitude) + '\n')
    else:
        showText(window, 'Jerk Nystagmus\n' +
                        'Fixation amplitude[°]: ' + str(nysHelp.fixationAmplitude) + '\n' +
                        'Gaze amplitude[°]: ' + str(nysHelp.gazeAmplitude) + '\n' +
                        'Vertical Movement[px]: ' + str(nysHelp.vertMovementPx) + '\n' +
                        'Frequency[Hz]: ' + str(nysHelp.freq) + '\n' +
                        'Direction: ' + nysHelp.direction)

    return False
