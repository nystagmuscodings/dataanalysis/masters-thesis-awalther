# Filename: NysExpHelpFunctions.py
#
# This file contains some helper methods, that are only used in the nystagmus experiment.
#
################################################################################
from psychopy import event, visual, core, gui
from psychopy.hardware import keyboard
import os
from numpy import genfromtxt
import random
from ExpUtils import preRecording, postRecording, showText
import pylink
from math import hypot
import collections
from statistics import mean, median
import time
################################################################################

# Initialize the keyboard.
keyBoard = keyboard.Keyboard()
# The median amplitude during fixation.
fixationAmplitude = 0
# The median amplitude when moving fixation.
gazeAmplitude = 0
# The median vertical movement during fixation in pixels.
vertMovementPx = 0
# The direction of the nystagmic saccades.
direction = 'unknown'
upperAngleBound = 0
lowerAngleBound = 0
# The frequency of the nystagmus.
freq = 0
# This flag indicates whether the subject is assumed to have pendular nystagmus.
pendularNys = False

expType = ""
useAlgorithm = True

useLandolts = False

subjectDataFolderPath = ""



##
# This method gets the information of the participant by displaying a gui.
#
# return -  [0] The name of the EDF file the data will be written to.
#               This encodes the SubjectNr as a one digit number, the trial ID as a one digit number,
#               'E' if Eyetest experiment or 'I' if image experiment, 'S' if static display content or 'C' if contingent display content.
#           [1] True, if the mouse shall be used as tracker.
#           [2] The experiment type. Either 'Eyetest' or 'Image'.
#           [3] True, if the display content shall be contingent. False if it shall be static.
##
def getExpInfo():
    info = {'SubjectNr': 0, 'Exp': ["Eyetest", "Image"], 'TrialNr': 0, 'Mouse_Simulation' : False, 'Static_Img' : False}
    # Display gui to let the experimenter enter the ID and name of the participant.
    dlg = gui.DlgFromDict(dictionary=info, title="Nystagmus experiment")
    if not dlg.OK: 
        core.quit()  # User pressed cancel.

    global expType
    expType = info['Exp']
    global useAlgorithm
    useAlgorithm = not info['Static_Img']

    global subjectDataFolderPath
    subjectDataFolderPath = os.getcwd() + '/subjectData/' + str(info['SubjectNr']) + '/' + expType + '/'
    if not os.path.exists(subjectDataFolderPath): 
        os.makedirs(subjectDataFolderPath)

    return str(info['SubjectNr']) + str(info['TrialNr']) + ('E' if str(info['Exp']) == "Eyetest" else 'I') + ('S' if info['Static_Img'] else 'C') + '.EDF', info['Mouse_Simulation'], expType, useAlgorithm


##
# This method displays the instructions of the experiment, which includes text
# and in case of the eyetest trials an example of the used eyetest sign.
# The displayed screen can be exited with any key press.
#
# param window - The window in which the instructions shall be displayed.
##
def displayInstructions(window):
    if expType == 'Eyetest':
        if useLandolts:
            instrText = visual.TextStim(window, 'Im Folgenden werden Ihnen schwarze Ringe präsentiert,' +
                                        'die an einer Seite eine Öffnung besitzen.\n\n\n\n\n\n\n\n' +
                                        'Gehen Sie die Ringe von Oben nach Unten durch.\n' +
                                        'Sprechen Sie laut aus, in welche Richtung die Öffnung zeigt.', color='black', height = 50, wrapWidth = 1400)
            imgPath = 'images' + os.sep + 'Bsp_LR_BottomLeft.png'
        else:
            instrText = visual.TextStim(window, 'Im Folgenden werden Ihnen schwarze Es präsentiert,' +
                                        'die in eine bestimmte Richtung zeigen.\n\n\n\n\n\n\n\n' +
                                        'Gehen Sie die Es von Oben nach Unten durch.\n' +
                                        'Sprechen Sie laut aus, in welche Richtung die Es zeigen.', color='black', height = 50, wrapWidth = 1400)
            imgPath1 = 'images' + os.sep + 'Bsp_E_Top.png'
            imgPath2 = 'images' + os.sep + 'Bsp_E_Right.png'
            imgPath3 = 'images' + os.sep + 'Bsp_E_Bottom.png'
            imgPath4 = 'images' + os.sep + 'Bsp_E_Left.png'
        exampImg1 = visual.ImageStim(window, imgPath1, units = 'pix', size = 200, pos = (-600,0))
        exampImg2 = visual.ImageStim(window, imgPath2, units = 'pix', size = 200, pos = (-200,0))
        exampImg3 = visual.ImageStim(window, imgPath3, units = 'pix', size = 200, pos = (200,0))
        exampImg4 = visual.ImageStim(window, imgPath4, units = 'pix', size = 200, pos = (600,0))
        exampImg1.draw()
        exampImg2.draw()
        exampImg3.draw()
        exampImg4.draw()

        topText = visual.TextStim(window, 'oben', color='black', height = 50, wrapWidth = 1400, pos = (-600,-130))
        rightText = visual.TextStim(window, 'rechts', color='black', height = 50, wrapWidth = 1400, pos = (-200,-130))
        bottomText = visual.TextStim(window, 'unten', color='black', height = 50, wrapWidth = 1400, pos = (200,-130))
        leftText = visual.TextStim(window, 'links', color='black', height = 50, wrapWidth = 1400, pos = (600,-130))
        topText.draw()
        rightText.draw()
        bottomText.draw()
        leftText.draw()
    elif expType == 'Image':
        instrText = visual.TextStim(window, 'Im Folgenden wird Ihnen für 20 Sekunden ein Gemälde präsentiert.\n' +
                                            'Bitte prägen Sie sich dieses so gut wie möglich ein.\n' +
                                            'Im Anschluss wird Ihnen ein Bildausschnitt gezeigt.\n' + 
                                            'Für den müssen Sie entscheiden, ob er zu dem nun folgendem Bild gehört oder nicht.', color='black', height = 50, wrapWidth = 1400)

    instrText.draw()
    window.flip()
    event.waitKeys()


##
# This method is used to create the needed trial data. It creates a set of random trial data,
# with two entries for each visual acuity, and a single data entry for the practise trial.
#
# return -  [1] : Random trial data.
#           [2] : Practise trial data.
##
def loadTrialData():
    # Load trail data from csv file.
    # The file contains the following columns:
    #                               DezVa: The dezimal visual acuity, that corresponds to the size of the eyetest sign.
    #                               ImgFile: The name of the image file that shall be displayed.
    #                               CorrectKeyTop: The expected key for the top eyetest sign.
    #                               CorrectKeyBottom: The expected key for the bottom eyetest sign.
    #                               List: The list this dataset belongs to. Used to split the dataset.
    if useLandolts:
        trialData = genfromtxt('./DataLandolt.csv', delimiter = ';', skip_header = 1, dtype = 'str')
        # Split the complete data into chunks of 24 entries, where each chunk will contain the data for one visual acuity.
        vaLists = [trialData[x : x+24] for x in range(0, len(trialData), 24)]
    else:
        trialData = genfromtxt('./DataSnellenE.csv', delimiter = ';', skip_header = 1, dtype = 'str')
        # Split the complete data into chunks of 12 entries, where each chunk will contain the data for one visual acuity.
        vaLists = [trialData[x : x+12] for x in range(0, len(trialData), 12)]
    randomTrialDataEyetest = []

    # Randomly choose two entries of each chunk.
    for vaList in vaLists:
        firstChoice = random.choice(vaList)
        randomTrialDataEyetest.append(firstChoice)
        secondChoice = random.choice(vaList)
        # Make sure that the second entry is another one than the first.
        while str(secondChoice[2]) == str(firstChoice[2]):
            secondChoice = random.choice(vaList)
        randomTrialDataEyetest.append(secondChoice)

    # Randomly choose one entry for visual acuity of 0.05 for the practise trial.
    practiseTrialData = []
    practiseTrialData.append(random.choice(vaLists[0]))

    # Load trial data for image trials.
    trialDataImg = genfromtxt('./DataImgTask.csv', delimiter = ';', skip_header = 1, dtype = 'str')
    randomTrialDataImg = []
    firstChoice = random.choice(trialDataImg)
    randomTrialDataImg.append(firstChoice)
    secondChoice = random.choice(trialDataImg)
    # Make sure that all entries are different from each other.
    while str(secondChoice[0]) == str(firstChoice[0]):
        secondChoice = random.choice(trialDataImg)
    randomTrialDataImg.append(secondChoice)
    thirdChoice = random.choice(trialDataImg)
    while str(thirdChoice[0]) == str(firstChoice[0]) or str(thirdChoice[0]) == str(secondChoice[0]):
        thirdChoice = random.choice(trialDataImg)
    randomTrialDataImg.append(thirdChoice)
    fourthChoice = random.choice(trialDataImg)
    while str(fourthChoice[0]) == str(firstChoice[0]) or str(fourthChoice[0]) == str(secondChoice[0]) or str(fourthChoice[0]) == str(thirdChoice[0]):
        fourthChoice = random.choice(trialDataImg)
    practiseTrialData.append(fourthChoice)

    return randomTrialDataEyetest, practiseTrialData, randomTrialDataImg


##
# This method is responsible for extracting the amplitude of the nystagmic saccades and
# the amplitude of the saccade when moving the fixation to another stimulus.
#
# param window - The window the stimulu shall be drawn to.
# param tracker - The tracker used to record the eye movements.
#
# return True, if the ESC-key has been pressed. False otherwise.
##
def extractParameters(window, tracker):
    showText(window, 'Im Folgenden werden einige Parameter Ihres Nystagmus bestimmt.\n' +
                    'Dazu werden Ihnen nacheinander rote Punkte präsentiert.\n' +
                    'Bitte versuchen Sie diese so gut wie möglich zu fixieren.')
    # Setup stimuli.
    stimulus = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[1,0,0], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    absStimPosY = int(window.monitor.getSizePix()[1] / 4)
    stimPositions = [(0, absStimPosY), (0, -absStimPosY), (0, absStimPosY), (0, -absStimPosY), (0, absStimPosY), (0, -absStimPosY)]
    stimulus.pos = stimPositions[0]
    # Setup stimulus for simulation.
    #simStim = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim.pos = stimPositions[0]
    #simPendular = False
    #moveRight = True
    # Buffer for saccade amplitudes during fixations.
    fixationAmpBuffer = collections.deque()
    # Buffer for saccade amplitudes when moveing fixation between stimuli.
    gazeAmpBuffer = collections.deque()
    # Buffer for vertical eye movements during fixation.
    verticalMoveBuffer = collections.deque()
    # Buffer for nystagmic saccade direction.
    directionBuffer = collections.deque()
    # Flag that indicates if a fixation is assumed.
    fixation = True
    # This flag indicates if the stimulus has been moved already since the timer elapsed.
    movedStimulus = False

    ##########     RECORDING - START     ##########

    preRecording(window, tracker, -1, (0, absStimPosY))

    stimulus.draw()
    #simStim.draw()
    window.flip()
    # This counter counts the number of stimuli that were already presented.
    stimCount = 0
    # A timer, that starts a countdown from 3 seconds.
    timer = core.CountdownTimer(3)
    # Clear all possible keyboard events, that may have occurred during calibration or driftcheck.
    keyBoard.clearEvents()

    # Will break after 6 stimuli were presented or the ESC-Key was pressed.
    while True:
        keys = keyBoard.getKeys(keyList = ['escape'], waitRelease=False)
        # Break out, if ESC was pressed.
        if 'escape' in keys:
            # Send over a message to log the key press.
            tracker.sendMessage('Pressed ESC. Exiting.')
            return True

        ev = tracker.getNextData()
        # Iterate over the eyetracker messages and check, if an end of a saccade has been detected.
        while ev != 0:
            # If the end of a saccade has been detected, append its amplitude to one of the buffers.
            if ev == pylink.ENDSACC:
                data = tracker.getFloatData()
                # Only use data of right eye.
                if data.getEye() == 1:
                    # Compute the amplitude of the saccade.
                    sacAmplitude = data.getAmplitude()
                    amp = hypot(sacAmplitude[0], sacAmplitude[1])
                    # If the to be fixated stimulus is presented, append the amplitude to the corresponding buffer.
                    if fixation:
                        if amp > 1:
                            fixationAmpBuffer.append(amp)
                            verticalMoveBuffer.append(abs(data.getStartGaze()[1] - data.getEndGaze()[1]))
                            directionBuffer.append(abs(data.getAngle()))
                    # If the stimulus positions is about to change, check if the detected saccades amplitude is much higher than the median amplitude during fixation.
                    # If so, the amplitude propably belongs to the change of stimulus fixation.
                    else:
                        gazeAmpBuffer.append(amp)
                    break
            ev = tracker.getNextData()

        # Display each stimulus for 3 seconds.
        timerTime = timer.getTime()
        if timerTime < 0.5 and timerTime > 0:
            fixation = False
        if timerTime < 0.01 and not movedStimulus:
            stimCount += 1
            if stimCount > 5:
                break
            stimulus.pos = stimPositions[stimCount]
            #simStim.pos = stimPositions[stimCount]
            movedStimulus = True
        if timerTime < -0.5:
            fixation = True
            timer.reset()
            movedStimulus = False

        #if simPendular:
        #    if moveRight:
        #        simStim.pos += (1, 0)
        #        if simStim.pos[0] > stimPositions[stimCount][0] + 50:
        #            moveRight = False
        #    else:
        #        simStim.pos -= (1, 0)
        #        if simStim.pos[0] < stimPositions[stimCount][0] - 50:
        #            moveRight = True
        #else:
        #    simStim.pos += (0.5, 0)
        #    if simStim.pos[0] > stimPositions[stimCount][0] + 50:
        #        simStim.pos = stimPositions[stimCount]

        stimulus.draw()
        #simStim.draw()
        window.flip()

    tracker.stopRecording()


    ##########     RECORDING - STOP     ##########

    # Extract the amplitude during fixation.
    # Check if any saccades occurred during fixation. If not, the subject has most probably pendular nystagmus.
    if fixationAmpBuffer:
        global fixationAmplitude
        fixationAmplitude = median(fixationAmpBuffer)

        # Extract the frequency.
        freqHelpBuffer = collections.deque()
        for amp in fixationAmpBuffer:
            if amp > fixationAmplitude * 0.5 and  amp < fixationAmplitude * 2:
                freqHelpBuffer.append(amp)
        global freq
        freq = len(freqHelpBuffer) / 15

        # Extract the direction.
        global direction
        global upperAngleBound
        global lowerAngleBound
        leftCount = 0
        rightCount = 0
        for angle in directionBuffer:
            if 135 < angle and angle < 180:
                leftCount += 1
            elif 0 < angle and angle < 45:
                rightCount += 1
        if rightCount < leftCount:
            direction = 'left'
            upperAngleBound = 180
            lowerAngleBound = 135
        else:
            direction = 'right'
            upperAngleBound = 45
            lowerAngleBound = 0

    # Check if pendular or jerk. Assume that a subject with pendular nystagmus will not
    # voluntary create saccades more than once in two seconds. One stimulus presentation lasts 2.5 seconds.
    # The complete extraction phase lasts 2.5 * 6 seconds, which leads to 2.5 * 6 / 2 acceptable voluntary
    # saccades during the extraction phase. Otherwise it is considered to be jerk nystagmus.
    global pendularNys
    pendularNys = not fixationAmpBuffer or len(fixationAmpBuffer) < 8

    # Extract the 6 highest values of the gaze amplitudes.
    if gazeAmpBuffer:
        gazeAmpHelpBuffer = collections.deque()
        while len(gazeAmpHelpBuffer) < 6:
            maxVal = max(gazeAmpBuffer)
            gazeAmpHelpBuffer.append(maxVal)
            gazeAmpBuffer.remove(maxVal)
        global gazeAmplitude
        gazeAmplitude = median(gazeAmpHelpBuffer)

    # Extract the vertical movement during fixation.
    if verticalMoveBuffer:
        global vertMovementPx
        vertMovementPx = median(verticalMoveBuffer)

    # Save data to file.
    calDataFileName = subjectDataFolderPath + 'calData' + expType + '.csv'
    f = open(calDataFileName, "w")
    f.write('Fixation Amplitudes: \n')
    for amp in fixationAmpBuffer:
        f.write(str(amp) + '\n')
    f.write('######################################################\n')
    f.write('Gaze Amplitudes: \n')
    for amp in gazeAmpBuffer:
        f.write(str(amp) + '\n')
    f.write('######################################################\n')
    f.write('Vertical Movements: \n')
    for mov in verticalMoveBuffer:
        f.write(str(mov) + '\n')
    f.write('######################################################\n')
    f.write('Angles: \n')
    for ang in directionBuffer:
        f.write(str(ang) + '\n')
    f.write('######################################################\n')
    f.write('Jerk or Pendular: ' + ('pendular' if pendularNys else 'jerk') + '\n' +
            'Fixation amplitude[°]: ' + str(fixationAmplitude) + '\n' +
            'Gaze amplitude[°]: ' + str(gazeAmplitude) + '\n' +
            'Vertical Movement[px]: ' + str(vertMovementPx) + '\n' +
            'Frequency[Hz]: ' + str(freq) + '\n' +
            'Direction: ' + direction)
    f.close()
    # Show results on screen.
    if pendularNys:
        showText(window, 'Pendular Nystagmus\n' +
                        'Gaze amplitude[°]: ' + str(gazeAmplitude) + '\n')
    else:
        showText(window, 'Jerk Nystagmus\n' +
                        'Fixation amplitude[°]: ' + str(fixationAmplitude) + '\n' +
                        'Gaze amplitude[°]: ' + str(gazeAmplitude) + '\n' +
                        'Vertical Movement[px]: ' + str(vertMovementPx) + '\n' +
                        'Frequency[Hz]: ' + str(freq) + '\n' +
                        'Direction: ' + direction)

    return False



##########     Eyetest TRIALS     ##########

##
# This method runs as many eyetest trials as there are entries in the given 'randomTrialData' parameter.
#
# param 'window'            - The window, the stimuli shall be displayed in.
# param 'trackingSys'       - The system used for contigency. This may be a mouse or an eyetracker.
# param 'screenCenter'      - The center coordinates of the screen.
# param 'useMouse'          - Flag that indicates whether a mouse is used as tracking system or not.
# param 'randomTrialData'   - The parameters for the trial, taken from the loaded csv file, which includes the following fields:
#                               DezVa: The dezimal visual acuity, that corresponds to the size of the eyetest sign.
#                               ImgFile: The name of the image file that shall be displayed.
#                               CorrectKeyTop: The expected key for the top eyetest sign.
#                               CorrectKeyBottom: The expected key for the bottom eyetest sign.
#                               List: The list this dataset belongs to. Used to split the dataset.
##
def doEyetestTrials(window, trackingSys, screenCenter, useMouse, randomTrialData):
    # Initialize the counters for correct and incorrect answers.
    correctCount = 0
    incorrectCount = 0
    incorrectCountBlock = 0
    estimatedVA = ""

    # Iterate over each of the random trial data set.
    for trial_index, trial_pars in enumerate(randomTrialData):
        # Run the trial.

        trialRes = runEyetestTrial(trial_pars, trial_index+1, window, trackingSys, screenCenter, useMouse)

        # Check if the experiment shall be stopped.
        if not trialRes:
            break

        # Update the correctness counters.
        correctCount += trialRes[0]
        incorrectCountBlock += trialRes[1]

        # Check if this is the second trial of the block.
        if trial_index % 2 == 1:
            estimatedVA = trial_pars[0]
            incorrectCount += incorrectCountBlock
            # If 2 or more answers were incorrect, stop the experiment.
            if incorrectCountBlock > 1:
                break
            else:
                incorrectCountBlock = 0

    # Display the results.
    showText(window, 'Richtige Antworten: ' + str(correctCount) + '\n' +
                        'Falsche Antworten: ' + str(incorrectCount) + '\n\n' + 
                        'Geschätzte Sehschärfe: ' + str(estimatedVA))


##
# This method defines the commands that are run for a single eyetest trial.
#
# param 'trialParameters'   - The parameters for the trial, taken from the loaded csv file, which includes the following fields:
#                               DezVa: The dezimal visual acuity, that corresponds to the size of the eyetest sign.
#                               ImgFile: The name of the image file that shall be displayed.
#                               CorrectKeyTop: The expected key for the top eyetest sign.
#                               CorrectKeyBottom: The expected key for the bottom eyetest sign.
#                               List: The list this dataset belongs to. Used to split the dataset.
# param 'trialIndex'        - The index of the current trial.
# param 'window'            - The window, the stimuli shall be displayed in.
# param 'trackingSys'       - The system used for contigency. This may be a mouse or an eyetracker.
# param 'screenCenter'      - The center coordinates of the screen.
# param 'useMouse'          - Flag that indicates whether a mouse is used as tracking system or not.
#
# return                    - The number of correct and incorrect answers of the trial or False, of the user pressed the ESC-key.
##
def runEyetestTrial(trialParameters, trialIndex, window, trackingSys, screenCenter, useMouse):
    # Unpacking the parameters.
    dezVA, imgFile, corKeyT, corKeyB, trialList = trialParameters
    mouse = event.Mouse(True, None, window)

    # Load the image to display, streched to fill full screen.
    if useLandolts:
        imgPath = 'images' + os.sep + 'LandoltRinge' + os.sep + dezVA + os.sep + imgFile
    else:
        imgPath = 'images' + os.sep + 'Snellen-E' + os.sep + dezVA + os.sep + imgFile
    #imgPath = 'images' + os.sep + 'TestImg.png'
    contImg = visual.ImageStim(window, imgPath, units='pix', size=(1920, 1080))
    staticImg = visual.ImageStim(window, imgPath, units='pix', size=(1920, 1080))
    #saccStartImg = visual.ImageStim(window, 'images' + os.sep + 'SaccStartImg.png', units='pix', size=(1920, 1080))
    saccStartImg = visual.ImageStim(window, imgPath, units='pix', size=(1920, 1080))
    imgPosBuffer = collections.deque(maxlen = 5)
    imgPosStorageBuffer = collections.deque()

    # Create stimuli for simulating horizontal jerk nystagmus.
    #simStim = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim.pos = (0, int(window.monitor.getSizePix()[1] / 4))
    #simStim1 = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim1.pos = (0, -int(window.monitor.getSizePix()[1] / 4))

    ##########     RECORDING - START     ##########

    if not useMouse:
        trackingSys.sendMessage('!V TRIAL_VAR dezVA %s' % dezVA)
        # Send some initial messages, perform a drift check and start recording.
        preRecording(window, trackingSys, trialIndex, (0, int(window.monitor.getSizePix()[1] / 4)))
        trackingSys.sendMessage('image_onset')
        # Send over a message to specify where the image is stored relative to the EDF data file.
        trackingSys.sendMessage('!V IMGLOAD CENTER %s %d %d' % ('..' + os.sep + imgPath, screenCenter[0], screenCenter[1]))
        if useAlgorithm:
            # Initialize the grabbingPoint.
            eyeData = trackingSys.getNewestSample().getRightEye().getGaze()
            grabbingPoint = (eyeData[0] - screenCenter[0], -eyeData[1] + screenCenter[1])
    else:
        mouseCircle = visual.Circle(window, radius=19, lineColor=(128, 128, 128), fillColor=(192, 192, 192), colorSpace='rgb')
        mouseCircle.draw()
        trackingSys.setPos(window.pos)
        # Initialize the grabbingPoint.
        grabbingPoint = window.pos

    # Display image.
    contImg.draw()
    #simStim.draw()
    #simStim1.draw()
    window.flip()

    # Initialize other variables.
    keyBoard.clearEvents()
    # Flag that indicates whether the static or the contingent image should be displayed.
    static = not useAlgorithm
    # Flag that indicates, if the start of a saccade has been detected.
    saccStart = False
    # The number of the eyetest sign, for which a key input is expected.
    signNumber = 0
    # Counters for correct and incorrect answers.
    correctCountTrial = 0
    incorrectCountTrial = 0

    # Display the image until 3 keys were pressed.
    while signNumber < 1:
        # Get the latest position information of the tracking system.
        if useMouse:
            trackingData = trackingSys.getPos()
        else:
            if useAlgorithm:
                eyeData = trackingSys.getNewestSample().getRightEye().getGaze()
                trackingData = (eyeData[0] - screenCenter[0], -eyeData[1] + screenCenter[1])

        # Check if a key was pressed and get the pressed key(s).
        keys = keyBoard.getKeys(keyList = ['escape', 'lctrl', 'e', 'i', 'q', 'r', 't', 'u', 'w', 'z'], waitRelease=False)
        # Break out, if ESC was pressed.
        if 'escape' in keys:
            if not useMouse:
                # Send over a message to log the key press.
                trackingSys.sendMessage('Pressed ESC. Exiting.')
            return False

        else:
            # Stop contingency if left mouse key is pressed and restart it, if it is released.
            if useAlgorithm:
                if mouse.getPressed()[0]:
                    static = True
                else:
                    if static:
                        grabbingPoint = (trackingData[0], trackingData[1])
                    static = False

            for curKey in keys:
                # If the left STRG-key was pressed, switch to static display.
                if curKey == 'lctrl':
                    if useAlgorithm:
                        static = not static
                        if not static:
                            grabbingPoint = (trackingData[0], trackingData[1])
                # If one of the answer keys has been pressed, evaluate the answer.
                elif curKey in ['e', 'i', 'q', 'r', 't', 'u', 'w', 'z']:
                    signNumber = (correctCountTrial + incorrectCountTrial) % 2
                    if not useMouse:
                        trackingSys.sendMessage('!V PRESSED KEY %s' % (curKey.name))
                    if signNumber == 0:
                        if not useMouse:
                            trackingSys.sendMessage('!V CORRECT KEY %s' % (corKeyT))
                        if curKey == corKeyT:
                            correctCountTrial += 1
                        else:
                            incorrectCountTrial += 1
                    elif signNumber == 1:
                        if not useMouse:
                            trackingSys.sendMessage('!V CORRECT KEY %s' % (corKeyB))
                        if curKey == corKeyB:
                            correctCountTrial += 1
                        else:
                            incorrectCountTrial += 1

        # If an eyetracker is used and we are in contingent mode:
        if useAlgorithm and not useMouse and not static:
            ev = trackingSys.getNextData()
            # Iterate over the eyetracker messages and check, if either a start or end of a saccade has been detected.
            while ev != 0:
                # If the start of a saccade has been detected, clear the buffer for the contImg position and switch to startSaccade mode.
                if ev == pylink.STARTSACC:
                    imgPosBuffer.clear()
                    saccStart = True
                # If the end of a saccade has been detected, check if its amplitude is way smaller or larger than the computed fixation amplitude or
                # if the angle is above or below the computed thresholds.
                # If so, the saccade is most propably a voluntary one. So reset the grabbing point.
                if ev == pylink.ENDSACC:
                    saccStart = False
                    data = trackingSys.getFloatData()
                    if data.getEye() == 1:
                        sacEndGaze = data.getEndGaze()
                        # If the nystagmus is jerky, we need to differentiate between voluntary and involuntary saccades.
                        if not pendularNys:
                            sacAmplitude = data.getAmplitude()
                            amp = hypot(sacAmplitude[0], sacAmplitude[1])
                            if amp < fixationAmplitude * 0.5 or amp > fixationAmplitude * 2 or abs(data.getAngle()) > upperAngleBound or abs(data.getAngle()) < lowerAngleBound:
                                grabbingPoint = (sacEndGaze[0] - screenCenter[0], -sacEndGaze[1] + screenCenter[1])
                                imgPosBuffer.clear()
                        # If the nystagmus is pendular, all saccades are voluntary.
                        else:
                            grabbingPoint = (sacEndGaze[0] - screenCenter[0], -sacEndGaze[1] + screenCenter[1])
                            imgPosBuffer.clear()
                    break
                ev = trackingSys.getNextData()

        # Display static image.
        if static:
            staticImg.draw()
        # Display saccade image.
        elif saccStart:
            saccStartImg.draw()
        # Display contingent image.
        else:
            # Compute the new theoretical position of the image and add it to the buffer.
            imgPosBuffer.append((trackingData[0] - grabbingPoint[0], trackingData[1] - grabbingPoint[1]))
            # Compute the actual position of the image by averaging the buffer data.
            pos = (mean(value[0] for value in imgPosBuffer), mean(value[1] for value in imgPosBuffer))
            if pos[0] < -screenCenter[0] or pos[0] > screenCenter[0] or pos[1] < -screenCenter[1] or pos[1] > screenCenter[1]:
                pos = screenCenter
            contImg.pos = pos
            contImg.draw()
        # If we use a mouse, display a circle at the mouse position.
        if useMouse:
            mouseCircle.pos = (trackingData[0], trackingData[1])
            mouseCircle.draw()

        #simStim.pos += (1, 0)
        #simStim1.pos += (1, 0)
        #if simStim.pos[0] > 200:
        #    simStim.pos = (0, int(window.monitor.getSizePix()[1] / 4))
        #    simStim1.pos = (0, -int(window.monitor.getSizePix()[1] / 4))
        #simStim.draw()
        #simStim1.draw()

        imgPosStorageBuffer.append((time.time(), contImg.pos))
        # Update the display.
        window.flip()

    ##########     RECORDING - END     ##########

    if not useMouse:
        postRecording(window, trackingSys)
    imgPosFilePath = subjectDataFolderPath + 'imgPos' + str(trialIndex) + '.csv'
    f = open(imgPosFilePath, "w")
    f.write('Image center coordinates: \n')
    for pos in imgPosStorageBuffer:
        f.write(str(pos[0]) + ';' + str(pos[1][0]) + ';' + str(pos[1][1])+ '\n')
    f.close()

    return correctCountTrial, incorrectCountTrial



##########     IMG TRIALS     ##########

##
# This method runs as many image trials as there is data in the given 'randomTrialData' parameter.
#
# param window          - The window everything shall be drawn into.
# param trackingSystem  - The system the contingent image shall respond to. This will either be the mouse or an eyetracker.
# param screenCenter    - The center coordinates of the screen.
# param useMouse        - Defines whether the mouse is used as tracking system.
# param trialParameters - The paths to the image and the snippet for the trial.
##
def doImgTrials(window, trackingSys, screenCenter, useMouse, randomTrialData):
    # Initialize the counters for correct and incorrect answers.
    correctCount = 0
    incorrectCount = 0

    # Iterate over each of the random trial data set.
    for trial_index, trial_pars in enumerate(randomTrialData):
        # Run the trial.
        trialRes = runImageTrial(window, trackingSys, screenCenter, useMouse, trial_pars)

        # Check if the experiment shall be stopped.
        if trialRes == -1:
            break

        # Update the correctness counters.
        if trialRes:
            correctCount += 1
        else:
            incorrectCount += 1

    # Display the results.
    showText(window, 'Richtige Antworten: ' + str(correctCount) + '\n' +
                    'Falsche Antworten: ' + str(incorrectCount) + '\n')


##
# This method handles the trial in which an image is displayed to the user.
# After 20 seconds the trial will be exited automatically.
#
# param window          - The window everything shall be drawn into.
# param trackingSystem  - The system the contingent image shall respond to. This will either be the mouse or an eyetracker.
# param screenCenter    - The center coordinates of the screen.
# param useMouse        - Defines whether the mouse is used as tracking system.
# param trialParameters - The paths to the image and the snippet for the trial.
#
# return - False, if the user pressed the ESC-key.
##
def runImageTrial(window, trackingSys, screenCenter, useMouse, trialParameters):
    # Unpacking the parameters.
    imgPath, snippetPath = trialParameters
    # Mouse instance for controling contingency.
    mouse = event.Mouse(True, None, window)

    # Compute the number of pixels that have to surround the image in order to prevent it from moving outside of the monitor boundaries.
    # It is assumed, that the monitor is 52cm width and the users eye is located 55cm infront of the monitor.
    # In this case one visual angle equals 10/7cm (measured empiricaly), which equals 51.75 pixels.
    # The computed fixation amplitude is increased by 25% to account for variations in amplitude.
    margin = fixationAmplitude * 1.25 * 51.75
    scaleFactor =  1 - ((2*margin) / 1920)
    contImg = visual.ImageStim(window, imgPath, units='pix', size=(1920 * scaleFactor, 1080 * scaleFactor))
    staticImg = visual.ImageStim(window, imgPath, units='pix', size=(1920 * scaleFactor, 1080 * scaleFactor))
    #saccStartImg = visual.ImageStim(window, 'images' + os.sep + 'SaccStartImg.png', units='pix', size=(1920 * scaleFactor, 1080 * scaleFactor))
    saccStartImg = visual.ImageStim(window, imgPath, units='pix', size=(1920 * scaleFactor, 1080 * scaleFactor))
    imgPosBuffer = collections.deque(maxlen = 5)
    imgPosStorageBuffer = collections.deque()

    # Create stimuli for simulating horizontal jerk nystagmus.
    #simStim = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim.pos = (0, int(window.monitor.getSizePix()[1] / 4))
    #simStim1 = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
    #simStim1.pos = (0, -int(window.monitor.getSizePix()[1] / 4))


    ##########     RECORDING - START     ##########

    if not useMouse:
        # Send some initial messages, perform a drift check and start recording.
        preRecording(window, trackingSys, 0, (0,0))
        trackingSys.sendMessage('image_onset')
        # Send over a message to specify where the image is stored relative to the EDF data file.
        trackingSys.sendMessage('!V IMGLOAD CENTER %s %d %d' % ('..' + os.sep + imgPath, screenCenter[0], screenCenter[1]))
        if useAlgorithm:
            # Initialize the grabbingPoint.
            eyeData = trackingSys.getNewestSample().getRightEye().getGaze()
            grabbingPoint = (eyeData[0] - screenCenter[0], -eyeData[1] + screenCenter[1])
    else:
        mouseCircle = visual.Circle(window, radius=19, lineColor=(128, 128, 128), fillColor=(192, 192, 192), colorSpace='rgb')
        mouseCircle.draw()
        trackingSys.setPos(window.pos)
        # Initialize the grabbingPoint.
        grabbingPoint = window.pos

    # Display image.
    contImg.draw()
    #simStim.draw()
    #simStim1.draw()
    window.flip()

    # Initialize other variables.
    keyBoard.clearEvents()
    # Flag that indicates whether the static or the contingent image should be displayed.
    static = not useAlgorithm
    # Flag that indicates, if the start of a saccade has been detected.
    saccStart = False

    timer = core.CountdownTimer(20)
    snippetOnset = False


    while True:
        if timer.getTime() < 0 and not snippetOnset:
            snippetOnset = True
            showText(window, 'Gehört dieser Bildausschnitt zu dem vorigen Bild?')
            snippetTargetStim = visual.Circle(window, radius=window.monitor.getSizePix()[0]/256, lineColor=[-1,-1,-1], fillColor=[1,1,1], lineWidth=window.monitor.getSizePix()[0]/256.0, units='pix')
            snippetTargetStim.pos = (0, 0)
            snippetTargetStim.draw()
            window.flip()
            contImg = visual.ImageStim(window, snippetPath, units='pix', size=(200 , 200))
            staticImg = visual.ImageStim(window, snippetPath, units='pix', size=(200, 200))
            saccStartImg = visual.ImageStim(window, snippetPath, units='pix', size=(200, 200))
            imgPosBuffer.clear()
            event.waitKeys()
            if not useMouse:
                if useAlgorithm:
                    trackingSys.sendMessage('snippet_onset')
                    eyeData = trackingSys.getNewestSample().getRightEye().getGaze()
                    grabbingPoint = (eyeData[0] - screenCenter[0], eyeData[1] - screenCenter[1])
            else:
                trackingSys.setPos(window.pos)
                # Initialize the grabbingPoint.
                grabbingPoint = window.pos

            # Display image.
            contImg.draw()
            #simStim.draw()
            #simStim1.draw()
            window.flip()


        # Get the latest position information of the tracking system.
        if useMouse:
            trackingData = trackingSys.getPos()
        else:
            if useAlgorithm:
                eyeData = trackingSys.getNewestSample().getRightEye().getGaze()
                trackingData = (eyeData[0] - screenCenter[0], -eyeData[1] + screenCenter[1])

        # Check if a key was pressed and get the pressed key(s).
        keys = keyBoard.getKeys(keyList = ['escape', 'lctrl', 'v', 'n'], waitRelease=False)
        # Break out, if ESC was pressed.
        if 'escape' in keys:
            if not useMouse:
                # Send over a message to log the key press.
                trackingSys.sendMessage('Pressed ESC. Exiting.')
            return -1
        elif 'lctrl' in keys:
            if useAlgorithm:
                # If the left STRG-key was pressed, switch to static display.
                static = not static
                if not static:
                    grabbingPoint = (trackingData[0], trackingData[1])

        # If snippet is displayed and the 'snipped-contained-in-img'-key is pressed,
        # check if the answer was correct and return True or False.
        elif 'v' in keys and snippetOnset:
            answerCorrect = 'w' not in snippetPath
            if not useMouse:
                trackingSys.sendMessage('!V ANSWER CORRECT %s' % (str(answerCorrect)))
            if useAlgorithm and not useMouse:
                trackingSys.stopRecording()
            return answerCorrect
        # If snippet is displayed and the 'snipped-not-contained-in-img'-key is pressed:
        # check if the answer was correct and return True or False.
        elif 'n' in keys and snippetOnset:
            answerCorrect = 'w' in snippetPath
            if not useMouse:
                trackingSys.sendMessage('!V ANSWER CORRECT %s' % (str(answerCorrect)))
            if useAlgorithm and not useMouse:
                trackingSys.stopRecording()
            return answerCorrect


        # Stop contingency if left mouse key is pressed and restart it, if it is released.
        if useAlgorithm:
            if mouse.getPressed()[0]:
                static = True
            else:
                if static:
                    grabbingPoint = (trackingData[0], trackingData[1])
                static = False

        # If an eyetracker is used and we are in contingent mode:
        if useAlgorithm and not useMouse and not static:
            ev = trackingSys.getNextData()
            # Iterate over the eyetracker messages and check, if either a start or end of a saccade has been detected.
            while ev != 0:
                # If the start of a saccade has been detected, clear the buffer for the contImg position and switch to startSaccade mode.
                if ev == pylink.STARTSACC:
                    imgPosBuffer.clear()
                    saccStart = True
                # If the end of a saccade has been detected, check if its amplitude is way smaller or larger than the computed fixation amplitude.
                # If so, the saccade is most propably a voluntary one. So reset the grabbing point.
                if ev == pylink.ENDSACC:
                    saccStart = False
                    data = trackingSys.getFloatData()
                    if data.getEye() == 1:
                        sacEndGaze = data.getEndGaze()
                        # If the nystagmus is jerky, we need to differentiate between voluntary and involuntary saccades.
                        if not pendularNys:
                            sacAmplitude = data.getAmplitude()
                            amp = hypot(sacAmplitude[0], sacAmplitude[1])
                            if amp < fixationAmplitude * 0.5 or amp > fixationAmplitude * 2 or abs(data.getAngle()) > upperAngleBound or abs(data.getAngle()) < lowerAngleBound:
                                grabbingPoint = (sacEndGaze[0] - screenCenter[0], -sacEndGaze[1] + screenCenter[1])
                                imgPosBuffer.clear()
                        # If the nystagmus is pendular, all saccades are voluntary.
                        else:
                            grabbingPoint = (sacEndGaze[0] - screenCenter[0], -sacEndGaze[1] + screenCenter[1])
                            imgPosBuffer.clear()
                    break
                ev = trackingSys.getNextData()

        # Display static image.
        if static:
            staticImg.draw()
        # Display saccade image.
        elif saccStart:
            saccStartImg.draw()
        # Display contingent image.
        else:
            # Compute the new theoretical position of the image and add it to the buffer.
            imgPosBuffer.append((trackingData[0] - grabbingPoint[0], trackingData[1] - grabbingPoint[1]))
            # Compute the actual position of the image by averaging the buffer data.
            pos = (mean(value[0] for value in imgPosBuffer), mean(value[1] for value in imgPosBuffer))
            if pos[0] < -screenCenter[0] or pos[0] > screenCenter[0] or pos[1] < -screenCenter[1] or pos[1] > screenCenter[1]:
                pos = screenCenter
            contImg.pos = pos
            contImg.draw()
        # If we use a mouse, display a circle at the mouse position.
        if useMouse:
            mouseCircle.pos = (trackingData[0], trackingData[1])
            mouseCircle.draw()

        #simStim.pos += (1, 0)
        #simStim1.pos += (1, 0)
        #if simStim.pos[0] > 200:
        #    simStim.pos = (0, int(window.monitor.getSizePix()[1] / 4))
        #    simStim1.pos = (0, -int(window.monitor.getSizePix()[1] / 4))
        #simStim.draw()
        #simStim1.draw()

        imgPosStorageBuffer.append(contImg.pos)
        # Update the display.
        window.flip()

    ##########     RECORDING - END     ##########

    if not useMouse:
        postRecording(window, trackingSys)
    imgPosFilePath = subjectDataFolderPath + 'imgPos' + str(trialIndex) + '.csv'
    f = open(imgPosFilePath, "w")
    f.write('Image center coordinates: \n')
    for pos in imgPosStorageBuffer:
        f.write(str(pos[0]) + ';' + str(pos[1]) + '\n')
    f.close()



##
# This method runs practise trials, until all answers were correct.
# It calls the runTrial()-method with the practiseTrialData, generated at the beginning of this document.
#
# param 'practiseTrialData'   - The parameters for the trial, taken from the loaded csv file, which includes the following fields:
#           for eyetest trials:
#                               DezVa: The dezimal visual acuity, that corresponds to the size of the eyetest sign.
#                               ImgFile: The name of the image file that shall be displayed.
#                               CorrectKeyTop: The expected key for the top eyetest sign.
#                               CorrectKeyBottom: The expected key for the bottom eyetest sign.
#                               List: The list this dataset belongs to. Used to split the dataset.
# param 'window'            - The window, the stimuli shall be displayed in.
# param 'trackingSys'       - The system used for contigency. This may be a mouse or an eyetracker.
# param 'screenCenter'      - The center coordinates of the screen.
# param 'useMouse'          - Flag that indicates whether a mouse is used as tracking system or not.
#
# return - False, if the user pressed the ESC-key. Nothing otherwise.
##
def runPractiseTrial(practiseTrialData, window, trackingSys, screenCenter, useMouse):
    showText(window, 'Dies ist ein Probedurchlauf.')

    if expType == 'Eyetest':
        correctCount = 0
        incorrectCount = 0
        while True:
            trialRes = runEyetestTrial(practiseTrialData[0], 0, window, trackingSys, screenCenter, useMouse)

            # Tell the main logic to quit the experiment.
            if not trialRes:
                return True

            # Display the results.
            showText(window, 'Richtige Antworten: ' + str(trialRes[0]) + '\n' +
                            'Falsche Antworten: ' + str(trialRes[1]))

            # Break the practise loop if all eyetest signs were answered correctly.
            if trialRes[1] < 1:
                break

            showText(window, 'Es müssen mindestens 2 Antworten richtig sein.')

    elif expType == 'Image':
        trialRes = runImageTrial(window, trackingSys, screenCenter, useMouse, practiseTrialData[1])

        # Tell the main logic to quit the experiment.
        if trialRes == -1:
            return True

        # Display the results.
        if trialRes:
            showText(window, 'Die Antwort war richtig.')
        else:
            showText(window, 'Die Antwort war falsch.')